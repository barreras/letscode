#include <stdio.h>
#include <stdlib.h>

#include "candles.h"

int main( int argc, char** argv ) {
  int n = 5;
  int heigths[5] = {19, 10, 8, 17, 9};
  int res = candles_to_blow(n, heigths);
  printf("%i blowed!\n", res);
  return 0;
}
